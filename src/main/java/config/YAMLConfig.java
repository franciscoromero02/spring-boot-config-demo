package config;
import java.util.List;

public interface YAMLConfig {
    public String getName();
    public void setName(String name);

    public String getEnvironment();
    public void setEnvironment(String environment);

    public List<String> getServers();
    public void setServers(List<String> servers);

    public int getWeight();
    public void setWeight(int weight);
}