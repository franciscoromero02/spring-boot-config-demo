package config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import java.util.ArrayList;
import java.util.List;

@Component
@ConfigurationProperties //(prefix = "")
@Validated
public class YAMLConfigImpl implements YAMLConfig {
    @NotEmpty
    private String name;
    @NotEmpty
    private String environment;
    @NotEmpty
    private List<String> servers = new ArrayList<>();
    @Max(5)
    @Min(0)
    private int weight;

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getEnvironment() {
        return this.environment;
    }

    @Override
    public void setEnvironment(String environment) {
        this.environment = environment;
    }

    @Override
    public List<String> getServers() {
        return this.servers;
    }

    @Override
    public void setServers(List<String> servers) {
        this.servers = servers;
    }

    @Override
    public int getWeight() {
        return this.weight;
    }

    @Override
    public void setWeight(int weight) { this.weight = weight; }
}